//
//  ViewController.swift
//  iTrainWreckTv
//
//  Created by Tord Wessman on 04/11/15.
//  Copyright © 2015 Axel IT AB. All rights reserved.
//

import UIKit

public func runWithDelay(delay: Double, closure: Void -> Void) {
    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
    dispatch_after(time, dispatch_get_main_queue(), closure)
}



class ViewController: UIViewController {

    private var server:ServerProtocol!
    
    @IBOutlet var accView:UIView!
    
    var sView:UIImageView!
    
    @IBOutlet var heartBeatView:HeartBeatView!
    @IBOutlet var strokeArnold:UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.accView.layer.borderColor = UIColor.blackColor().CGColor
        self.accView.layer.borderWidth = 2.0;
        
        let imageName = "sch.jpg"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        
        imageView.frame = CGRect(0, 0, width: self.accView.frame.width, self.accView.frame.height)
        self.sView = imageView
        self.accView.addSubview(self.sView)
        
        let addr = "10.10.9.67"
        let port = 8080
        
        strokeArnold.frame = heartBeatView.frame
        
        server = Server(listenIp: addr, listenPort: port)
        
        server.heartBeat { (val) -> () in
  
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                
            self.heartBeatView.addBeat(CGFloat(val))

                
        })
        
        
        }
        server.acceleration { (val) -> () in
            self.accView.frame = CGRectMake(self.accView.frame.origin.x, self.accView.frame.origin.y, self.accView.frame.width, CGFloat(val))
            self.accView.frame = CGRectMake(self.accView.frame.origin.x, 600 - abs(CGFloat(val) * 300), self.accView.frame.width, abs(CGFloat(val)) * 300)
            self.sView.frame = CGRectMake(0, 0, self.accView.frame.width, self.accView.frame.height)
        }
        server.start()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

