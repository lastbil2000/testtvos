//
//  Servecr.swift
//  iTrainWreckTv
//
//  Created by Tord Wessman on 04/11/15.
//  Copyright © 2015 Axel IT AB. All rights reserved.
//

import Foundation

public class Server : ServerProtocol {
    
    private var tcpServer:TCPServer!
    
    private var heartBeatDelegate:((Int) -> ())?
    private var accelerationDelegate:((Float) -> ())?
    
    private var running:Bool = false
    private var port:Int
    private var ip:String
    init (listenIp:String, listenPort:Int) {
        
        
        ip = listenIp
        port = listenPort
        
        tcpServer = TCPServer(addr: self.ip, port: self.port)
        
    }
    
    
    func heartBeat(delegate: (Int) -> ()) -> Self {
        
        heartBeatDelegate = delegate
        
        return self
    }
    
    func acceleration(delegate: (Float) -> ()) -> Self {
        
        accelerationDelegate = delegate
        return self
    }
    
    func start() {
        
        running = true
        
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            
            

                let (success,msg)=self.tcpServer.listen()
                
                if success {

                        if let client = self.tcpServer.accept() {
                        
                            while self.running {
                                
                                if let len = client.read(1) {
                                    
                                    if let data = client.read(Int(len[0])) {
                                        
                                        if let str = NSString(bytes: data, length: Int(len[0]),  encoding: NSUTF8StringEncoding) as? String {
                                            
                                            let arr = str.characters.split {$0 == ","}.map(String.init)
                                            
                                            let type = WreckValueType(rawValue: UInt8(arr[0])!)
                                            
                                            if type == .HeartBeat {
                                                self.heartBeatDelegate?(Int(arr[1])!)
                                            } else if type == .Acc {
                                                self.accelerationDelegate?(Float(arr[1])!)
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                
                            }
                            
                        } else {
                            
                            print ("ARGH. no client")
                        }
           

                } else{
                    
                    print(msg)
                    
                    self.running = false
                }
 
        })
        
        running = true
        
    }
    func stop() {
        
        running = false

       tcpServer.close()
    }

}