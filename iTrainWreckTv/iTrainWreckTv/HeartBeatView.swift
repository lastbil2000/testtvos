//
//  HeartBeatView.swift
//  iTrainWreckTv
//
//  Created by Tord Wessman on 04/11/15.
//  Copyright © 2015 Axel IT AB. All rights reserved.
//

import UIKit

class HeartBeatView: UIView {
    
    private var beats:[CGFloat] = []
    
    private let dotSpacing:CGFloat = 5
    
    private let lineWidth:CGFloat = 4.0
    
    func addBeat(beat: CGFloat) {
        
        let maxPoints:Int = Int(self.frame.width / dotSpacing)
        
        beats.append(beat)
        
        //Make sure only relevant elements fit
        if beats.count > maxPoints {
            beats.removeAtIndex(0)
        }
        
        self.setNeedsDisplay()
    }
    
    override func drawRect(rect: CGRect)
    {
        
        let context = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(context, lineWidth)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let components: [CGFloat] = [1.0, 1.0, 1.0, 0.6]
        let color = CGColorCreate(colorSpace, components)
        
        CGContextSetStrokeColorWithColor(context, color)
        
        var i:CGFloat = 0
        
        if beats.count > 0 {
            
            CGContextMoveToPoint(context, 0, self.frame.height - beats.first!)
            
            for beat in beats {
                i++
                
                CGContextAddLineToPoint(context, i * dotSpacing, self.frame.height  - beat)
                CGContextStrokePath(context)
                
                CGContextMoveToPoint(context, i * dotSpacing, self.frame.height - beat )
            }

        }
        
    }
}