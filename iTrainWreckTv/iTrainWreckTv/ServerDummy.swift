//
//  ServerDummy.swift
//  iTrainWreckTv
//
//  Created by Tord Wessman on 04/11/15.
//  Copyright © 2015 Axel IT AB. All rights reserved.
//

import Foundation

public class ServerDummy : NSObject, ServerProtocol {
    
    private var heartBeatDelegate:((Int) -> ())?
    private var accelerationDelegate:((Float) -> ())?
    private var timer:NSTimer!
    
    private var count:Double = 0

    func heartBeat(delegate: (Int) -> ()) -> Self {
        
        heartBeatDelegate = delegate
        
        return self
    }
    
    func acceleration(delegate: (Float) -> ()) -> Self {
        
        accelerationDelegate = delegate
        return self
    }
    
    func start() {
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("updateValues"), userInfo: nil, repeats: true)
    }
    
    public func updateValues() {
        
        count += 1
        
        accelerationDelegate?(Float(cos((count/180.0) * 3.14) + 1.0) * 20)
        
        heartBeatDelegate?(Int(sin((count/180.0) * 3.14) + 1.0) * 50)
        
    }
    
    func stop() {
        
        timer?.invalidate()
    }
    
}