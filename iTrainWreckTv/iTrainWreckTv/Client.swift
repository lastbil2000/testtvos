//
//  Client.swift
//  iTrainWreckTv
//
//  Created by Tord Wessman on 04/11/15.
//  Copyright © 2015 Axel IT AB. All rights reserved.
//

import Foundation

public enum WreckValueType : UInt8 {
    case HeartBeat = 1
    case Acc = 2
}

public class Client {

    private var tcpClient:TCPClient
    private(set) var ready:Bool = false
    
    init (addr:String, port:Int) {

        tcpClient = TCPClient(addr: addr, port: port)
        
        let (success1,errmsg1) = tcpClient.connect(timeout: 2)
        
        if success1 {
            ready = true
        } else {
            print(errmsg1)
        }
        


    }
    
    func send<T>(type: WreckValueType, value: T) -> Bool {
        
        defer {
            ready = true
        }
        
        assert(ready, "ARGH@! NOT CONNECTED!")
        ready = false

        let str = [UInt8](String("\(type.rawValue),\(value)").utf8)
        var buff: [UInt8] = [UInt8(str.count)]
        buff += str
        let (success,errmsg) = tcpClient.send(data: buff)

        if !success {
            print(errmsg)
        }
        
        return success

    }
}