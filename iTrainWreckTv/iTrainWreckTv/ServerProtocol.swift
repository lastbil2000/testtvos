//
//  ServerProtocol.swift
//  iTrainWreckTv
//
//  Created by Tord Wessman on 04/11/15.
//  Copyright © 2015 Axel IT AB. All rights reserved.
//

import Foundation

protocol ServerProtocol {
    
    func heartBeat(delegate: (Int) -> ()) -> Self
    func acceleration(delegate: (Float) -> ()) -> Self
    
    func start()
    func stop()
}